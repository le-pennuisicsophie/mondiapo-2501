Diaporama par slp

Hébergé sur forge.apps.education.fr

<!--

Choisissez ci-dessous la licence de votre diaporama
en ajoutant un x entre les crochets de votre choix :

[ ] Tout droit réservé
[ ] CC BY NC ND
[ ] CC BY ND
[x] CC BY NC SA
[ ] CC BY SA	
[ ] CC BY NC	
[ ] CC BY	
[ ] CC ZERO	

Pour savoir quelle licence choisir, consultez ce tableau :
https://link.infini.fr/licencescc

La licence choisie sera automatiquement affichée dans
l'onglet "A propos" du menu de votre diaporama.

-->
